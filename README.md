Ce dépôt contient la charte de notre groupe

# Qui et comment on peut modifier ces documents
- Créer un compte sur framagit (utilisation d'un pseudo possible et encouragé)
- Commencer par ouvrir ou répondre à une "issue" 
- Créer une branche et demander de la fusionner avec le dépôt (on peut imaginer un moyen de validation démocratique)
- Se joindre à la communauté des personne gérant le dépôt (attention personne plus expérimentée)

# Utilisation du Markdown
C'est pour cette raison qu'il y a un ".md" après le nom du fichier. C'est l'extension qui signifie que le fichier suit les conventions Markdown.

# Grammaire markdown
On utilise un dièze pour le premier niveau de titre. Et n dièzes pour les n suivant.
Pour écrire des listes on utilise des tirets.
