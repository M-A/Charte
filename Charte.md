# But
Présentez une liste aux prochaines élections municipales 2020 à Lille, pour un seul et unique mandat. 
Pour constituer la liste on utilise la chance pour désigner dans un groupe à définir

# Presentiel
Chaque personne est celle qu'il faut pour le bon déroulement de la réunion. Les gens s'écoutent et se parlent sereinement. On valide toutes les modifications de la charte en presentiel par consensus.

## Designer les personnes qui modèrent nos réunions
Une première personne lance un dé pour désigner la personne qui modère et on retourne un sablier. Quand le sablier se termine ou si la personne qui modère démissionne avant elle relance dans les deux cas pour désigner à nouveau. Ainsi le pouvoir tourne

## Rôle de la personne qui modère 
Elle donne la parole aux personnes qui la demande en levant la main. Elle peut aussi demander son avis aux personnes silencieuses. Une personne qui modère peut nommer une personne qui écrit
un premier brouillon de compte rendu de la réunion. Elle nomme les spécialistes et non le contraire. Ces spécialistes peuvent rester d'un mandat à un autre.
te, ainsi la personne qui parle peut le faire sans interuption. On peut aussi faire le symbole du temps mort pour annoncer une chose pratique, e.g. qui offre la prochaine tournée de jus.

# Informatique
On utilise des outils libres et faciles d'accés à tous.

## Gitlib
On l'utilise pour rédiger nos documents et compte-rendus. 

## AgoraKit
On l'utilise pour organiser nos rencontres et discuter des sujets pratiques. 

## Mastodon
On l'utilise pour promouvoir nos activités.



